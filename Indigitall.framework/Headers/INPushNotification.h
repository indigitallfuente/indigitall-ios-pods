#import <Foundation/Foundation.h>

@interface INPushNotification : NSObject

// MARK: - Properties
@property (nonatomic, retain) NSNumber *type;
@property (nonatomic, retain) NSNumber *subtype;
@property (nonatomic, retain) NSString *message_id;
@property (nonatomic, retain) NSString *appToken;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *body;
@property (nonatomic, retain) NSString *iconURL;
@property (nonatomic, retain) NSString *imageURL;
@property (nonatomic, retain) NSString *data;
@property (nonatomic, retain) NSString *action;
@property (nonatomic, retain) NSString *descCampaign;
@property (nonatomic) BOOL destructive;
@property (nonatomic, retain) NSArray *tagsToSubscribe;
@property (nonatomic, retain) NSArray *buttons;

@property (nonatomic) BOOL launch;

@property (nonatomic, retain) NSString *urlButton1;
@property (nonatomic, retain) NSString *titleButton1;
@property (nonatomic, retain) NSString *urlButton2;
@property (nonatomic, retain) NSString *titleButton2;
@property (nonatomic, retain) NSNumber *category;

-(id) initWithDictionary:(NSDictionary*) notification;

@end
