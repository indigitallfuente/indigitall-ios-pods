#import <Foundation/Foundation.h>

@interface INTag : NSObject

@property (nonatomic, retain) NSString * tagId;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * parent;
@property (nonatomic) BOOL selected;

-(id) initWithDictionary:(NSMutableDictionary*) jsonObject;
-(id) initSubscribedTagWithDictionary:(NSMutableDictionary*) jsonObject;

@end
