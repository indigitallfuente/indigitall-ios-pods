#import <UIKit/UIKit.h>

//! Project version number for Indigitall.
FOUNDATION_EXPORT double IndigitallVersionNumber;

//! Project version string for Indigitall.
FOUNDATION_EXPORT const unsigned char IndigitallVersionString[];


/// Pbulic methods
#import "IndigitallLib.h"
/// Public model
#import "INTag.h"
#import "INPushNotification.h"

