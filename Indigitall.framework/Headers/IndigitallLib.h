//
//  IndigitallLib.h
//  Indigitall
//
//  Created by Fernando on 17/3/17.
//  Copyright © 2017 Indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INPushNotification.h"

@protocol IndigitallDelegate <NSObject>
- (void)manageINPushNotification:(INPushNotification *)notification;
@optional
@end

@interface IndigitallLib : NSObject
///
+ (IndigitallLib *) sharedInstance;

/// Inicialization
- (void) setGroupName: (NSString *)groupName;
- (void) customDomain:(NSString *)domain;
- (void) initializeWithAppToken:(NSString *)appToken thirdPartyApps: (BOOL) thirdPartyApps;

/// PushNotification
- (void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void) didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken completionHandler:(void (^)(BOOL registerSuccess, NSError * error)) completionHandler;
- (void) didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;

- (void) didReceiveRemoteNotification:(NSDictionary *)userInfo;
- (void) handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)notification completionHandler:(void(^)())completionHandler;

/// Device Status
/**
 Lanza una consulta para comprobar si el dispositivo tiene el servicio activado
 EL bloque devuelve un YES si el servicio está activada, y NO si no lo está.
 */
- (void) checkDeviceStatusWithHandler:(void (^)(BOOL enabled, NSError * error)) completionHandler;
/**
 Activa el dispositivo
 */
- (void) enableDeviceWithHandler:(void (^)(NSError * error)) completionHandler;
/**
 Desactiva el dispositivo
 */
- (void) disableDeviceWithHandler:(void (^)(NSError * error)) completionHandler;

- (void) managePush:(INPushNotification *)notification;
@property (nonatomic, weak) id <IndigitallDelegate> delegate;


/// Localization
/** Si el valor es YES (por defecto), se enviarán actualizaciones del cambio de posición del usuario siempre que haya un cambio significativo y haya transcurrido
 un intervalo de tiempo mayor que el valor de `minimumLocationTimeInterval` desde la última petición.
 
 
 Si es NO, solo se enviará la posición la primera vez
 **/
- (BOOL) checkLocationStatus;
- (void) enableLocation;
- (void) disableLocation;
@property (nonatomic) BOOL sendCountinousLocationsUpdate;

/// Número mínimo de segundos que deben transcurrir entre dos actualizaciones de posición. Por deffecto es 120 (2 min)
@property (nonatomic) NSTimeInterval minimumLocationTimeInterval;

/// Tags
/**
 Devuelve todas las tags disponibles para esta app en el parámetro tags del completion handler. Si hay algun error, aparece en el parámetro error.
 */
- (void) tagsListWithHandler:(void (^)(NSArray * tags, NSError * error)) completionHandler;

/**
 Devuelve todas las tags a los que el dispositivo actual está subscrito en el parámetro tags del completion handler. Si hay algun error, aparece en el parámetro error.
 */
- (void) tagsSubscriptionsWithHandler:(void (^)(NSArray * tags, NSError * error)) completionHandler;
/**
 La librería solicita subscribirse a las notificaciones dadas.
 \param tagsIds Un NSArray con los ids (en NSString) de las notificaciones a las que suscribirse.
 */
- (void) tagsSubscribeToTags:(NSArray *)tagsIds completionHandler:(void (^)(NSError * error)) completionHandler;
/**
 La librería solicita desubscribirse a las notificaciones dadas.
 \param tagsIds Un NSArray con los ids (en NSString) de las notificaciones de las que desuscribirse.
 */
- (void) tagsUnsubscribeFromTags:(NSArray *)tagsIds completionHandler:(void (^)(NSError * error)) completionHandler;

/// SDK Utils
+ (NSString *) getIndigitallSDKVersion;
+ (NSString *) getDeviceID;
+ (NSString *) getApptoken;
+ (NSString *) getEnviroment;
+ (NSString *) getURL;

@end
