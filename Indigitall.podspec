Pod::Spec.new do |s|
    s.name              = 'Indigitall'
    s.version           = '3.3.7'
    s.summary           = 'indigitall\'s SDK for iOS'
    s.homepage          = 'https://indigitall.net/'

    s.author            = { 'Name' => 'indigitall' }
    #s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://bitbucket.org/indigitallfuente/indigitall-ios-pods', :tag => s.version.to_s }

    s.ios.deployment_target = '8.0'
    s.ios.vendored_frameworks = 'Indigitall.framework'
end
